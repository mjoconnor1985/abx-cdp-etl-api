﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AutoMapper;
using CDP.ETL.Data;
using CDP.ETL.Domain.RBA;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.Logging.AzureAppServices;
using CDP.ETL.Data.Chug;
using Microsoft.EntityFrameworkCore;
using CDP.ETL.Domain;
using CDP.ETL.WebApi.Infrastructure;

namespace CDP.ETL
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ILoggerFactory loggerFactory = new LoggerFactory();

            services.AddCors(builder => builder.AddPolicy("CorsPolicy", policy => policy.AllowAnyOrigin()));

            // Add framework services.
            services.AddMvc(config => config.Filters.Add(new GlobalExceptionFilter(loggerFactory, Configuration)));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("EtlApi", new Info { Title = "CDP ETL Web API", Version = "v1" });
            });

            services.AddAutoMapper();

            services.AddDbContext<ChugMobileContext>(opt => opt
            .UseSqlServer(Configuration.GetConnectionString("ChugMobileDb"), sqlOptions => sqlOptions.EnableRetryOnFailure()));

            services.AddSingleton<IConfiguration>(Configuration);

            services.AddScoped<IRBADataSource, RBADataSource>();
            services.AddScoped<IChugMobileDataSource, ChugMobileDataSource>();
            services.AddScoped<IRBAService, RBAService>();
            services.AddScoped<IChugMobileEtlService, ChugMobileEtlService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            loggerFactory.AddFile("ErrorLog-{Date}.txt", LogLevel.Error);
            loggerFactory.AddAzureWebAppDiagnostics(
             new AzureAppServicesDiagnosticsSettings
             {
                 OutputTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss zzz} [{Level}] {RequestId}-{SourceContext}: {Message}{NewLine}{Exception}"
             }
           );

            if (!env.IsProduction())
            {
                app.UseDeveloperExceptionPage();
            }
            
            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                Authority = Configuration["Authentication:AzureAd:AADInstance"] + Configuration["Authentication:AzureAd:TenantId"],
                Audience = Configuration["Authentication:AzureAd:Audience"]
            });

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/EtlApi/swagger.json", "CDP ETL Web API");
            });
        }
    }
}
