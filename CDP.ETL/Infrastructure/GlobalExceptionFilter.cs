﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.Extensions.Configuration;
using MailKit.Net.Smtp;
using MimeKit;

namespace CDP.ETL.WebApi.Infrastructure
{
    public class GlobalExceptionFilter: ExceptionFilterAttribute
    {
        ILogger _logger;
        IConfiguration _config;

        public GlobalExceptionFilter(ILoggerFactory logger, IConfiguration config)
        {
            _logger = logger.CreateLogger<GlobalExceptionFilter>();
            _config = config;
        }

        public override void OnException(ExceptionContext context)
        {
            var message = $"{context.Exception.Message} {Environment.NewLine} {context.Exception.StackTrace}";
            _logger.LogError(message);
            SendMail(message);
        }

        void SendMail(string errorMessage)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(_config["Smtp:From"]));
            message.To.Add(new MailboxAddress(_config["Smtp:To"]));
            message.Subject = "RBA Data Ingestion Failure";
            message.Body = new TextPart("plain")
            {
                Text = errorMessage
            };

            using (var client = new SmtpClient())
            {
                client.Connect(_config["Smtp:Server"], int.Parse(_config["Smtp:Port"]), false);
                client.Send(message);
                client.Disconnect(true);
            }
        }
    }
}
