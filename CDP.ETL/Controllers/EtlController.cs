﻿using CDP.ETL.Domain;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDP.ETL.WebApi.Controllers
{
    [Route("api/etl")]
    public class EtlController : Controller
    {
        #region Fields

        IChugMobileEtlService _chugEtlService;

        #endregion

        #region Constructors

        public EtlController(IChugMobileEtlService chugEtlService)
        {
            _chugEtlService = chugEtlService;
        }

        #endregion

        #region Methods

        [HttpGet]
        [Route("rbatochug")]
        public async Task<IActionResult> ExecuteRbaToChugEtlProcessAsync(DateTime? date)
        {
            if (date == null)
                date = DateTime.Now;

            var rowsSave = await _chugEtlService.ExecuteRbaToChugMobileEtlAsync((DateTime)date);

            return Created("", $"{rowsSave} rows saved.");
        }
        #endregion
    }
}
