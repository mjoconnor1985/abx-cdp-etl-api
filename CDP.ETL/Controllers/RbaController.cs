﻿using CDP.ETL.Domain.RBA;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDP.ETL.WebApi.Controllers
{
    [Route("api/rba")]
    public class RbaController: Controller
    {
        #region Fields

        IRBAService _rbaService;

        #endregion

        #region Constructors

        public RbaController(IRBAService rbaService)
        {
            _rbaService = rbaService;
        }

        #endregion

        #region MyRegion

        //[HttpGet]
        //[Route("saveConsumables")]
        //public async Task<IActionResult> SaveConsumablesAsync(DateTime? date)
        //{
        //    if (date == null)
        //        date = DateTime.Now;

        //    var consumables = await _rbaService.GetConsumablesAsync((DateTime)date);

        //    return Ok(consumables);
        //}

        #endregion
    }
}
