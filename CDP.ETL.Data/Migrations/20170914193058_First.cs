﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CDP.ETL.Data.Migrations
{
    public partial class First : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Operator",
                columns: table => new
                {
                    OperatorId = table.Column<string>(nullable: false),
                    Crew = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    Group = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NickName = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operator", x => x.OperatorId);
                });

            migrationBuilder.CreateTable(
                name: "SupportingEquipment",
                columns: table => new
                {
                    SupportingEquipmentId = table.Column<string>(nullable: false),
                    Capacity = table.Column<int>(nullable: false),
                    Equipment = table.Column<string>(nullable: true),
                    MachineId = table.Column<string>(nullable: true),
                    Operation = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SupportingEquipment", x => x.SupportingEquipmentId);
                });

            migrationBuilder.CreateTable(
                name: "Equipment",
                columns: table => new
                {
                    EquipmentId = table.Column<string>(nullable: false),
                    Capacity = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    EquipmentEquipment = table.Column<string>(nullable: true),
                    MachineId = table.Column<string>(nullable: true),
                    Operation = table.Column<string>(nullable: true),
                    OperatorId = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipment", x => x.EquipmentId);
                    table.ForeignKey(
                        name: "FK_Equipment_Operator_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operator",
                        principalColumn: "OperatorId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Location",
                columns: table => new
                {
                    LocationId = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    LocationHeadingName = table.Column<string>(nullable: true),
                    LocationName = table.Column<string>(nullable: true),
                    OperatorId = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Location", x => x.LocationId);
                    table.ForeignKey(
                        name: "FK_Location_Operator_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operator",
                        principalColumn: "OperatorId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EquipmentShift",
                columns: table => new
                {
                    EquipmentShiftId = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<string>(nullable: true),
                    DelayUnplannedHours = table.Column<decimal>(nullable: true),
                    EquipmentId = table.Column<string>(nullable: true),
                    OperationalHours = table.Column<decimal>(nullable: true),
                    OperatorId = table.Column<string>(nullable: true),
                    Shift = table.Column<string>(nullable: true),
                    ShiftDate = table.Column<DateTime>(nullable: false),
                    StandbyAutoHours = table.Column<decimal>(nullable: true),
                    StandbyUnplannedHours = table.Column<decimal>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EquipmentShift", x => x.EquipmentShiftId);
                    table.ForeignKey(
                        name: "FK_EquipmentShift_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "Equipment",
                        principalColumn: "EquipmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_EquipmentShift_Operator_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operator",
                        principalColumn: "OperatorId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SessionHistory",
                columns: table => new
                {
                    SessionHistoryId = table.Column<string>(nullable: false),
                    AutoEnd = table.Column<bool>(nullable: false),
                    AutoEndReason = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    EquipmentId = table.Column<string>(nullable: true),
                    LogOutTime = table.Column<DateTime>(nullable: true),
                    LoginTime = table.Column<DateTime>(nullable: true),
                    OperatorId = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SessionHistory", x => x.SessionHistoryId);
                    table.ForeignKey(
                        name: "FK_SessionHistory_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "Equipment",
                        principalColumn: "EquipmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SessionHistory_Operator_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operator",
                        principalColumn: "OperatorId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Task",
                columns: table => new
                {
                    TaskId = table.Column<string>(nullable: false),
                    ActualEndDate = table.Column<DateTime>(nullable: false),
                    ActualQuantity = table.Column<int>(nullable: false),
                    ActualStartDate = table.Column<DateTime>(nullable: false),
                    AutoEnd = table.Column<bool>(nullable: false),
                    AutoEndReason = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    Destination = table.Column<string>(nullable: true),
                    EquipmentId = table.Column<string>(nullable: true),
                    IsActivityComplete = table.Column<bool>(nullable: false),
                    LocationId = table.Column<string>(nullable: true),
                    Material = table.Column<string>(nullable: true),
                    Operation = table.Column<string>(nullable: true),
                    OperatorId = table.Column<string>(nullable: true),
                    OriginalOperation = table.Column<string>(nullable: true),
                    PlannedEndDate = table.Column<DateTime>(nullable: false),
                    PlannedQuantity = table.Column<int>(nullable: false),
                    PlannedStartDate = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Shift = table.Column<string>(nullable: true),
                    ShiftDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    SupervisorId = table.Column<string>(nullable: true),
                    SupportingEquipmentId = table.Column<string>(nullable: true),
                    SupportingOperation = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Task", x => x.TaskId);
                    table.ForeignKey(
                        name: "FK_Task_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "Equipment",
                        principalColumn: "EquipmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Task_Location_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Task_Operator_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operator",
                        principalColumn: "OperatorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Task_Operator_SupervisorId",
                        column: x => x.SupervisorId,
                        principalTable: "Operator",
                        principalColumn: "OperatorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Task_SupportingEquipment_SupportingEquipmentId",
                        column: x => x.SupportingEquipmentId,
                        principalTable: "SupportingEquipment",
                        principalColumn: "SupportingEquipmentId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    ActivityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Action = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    EquipmentShiftId = table.Column<string>(nullable: true),
                    OperatorId = table.Column<string>(nullable: true),
                    PerformedAt = table.Column<DateTime>(nullable: false),
                    Reference = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.ActivityId);
                    table.ForeignKey(
                        name: "FK_Activity_EquipmentShift_EquipmentShiftId",
                        column: x => x.EquipmentShiftId,
                        principalTable: "EquipmentShift",
                        principalColumn: "EquipmentShiftId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Activity_Operator_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operator",
                        principalColumn: "OperatorId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activity_EquipmentShiftId",
                table: "Activity",
                column: "EquipmentShiftId");

            migrationBuilder.CreateIndex(
                name: "IX_Activity_OperatorId",
                table: "Activity",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipment_OperatorId",
                table: "Equipment",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentShift_EquipmentId",
                table: "EquipmentShift",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_EquipmentShift_OperatorId",
                table: "EquipmentShift",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Location_OperatorId",
                table: "Location",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_SessionHistory_EquipmentId",
                table: "SessionHistory",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_SessionHistory_OperatorId",
                table: "SessionHistory",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Task_EquipmentId",
                table: "Task",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Task_LocationId",
                table: "Task",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Task_OperatorId",
                table: "Task",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Task_SupervisorId",
                table: "Task",
                column: "SupervisorId");

            migrationBuilder.CreateIndex(
                name: "IX_Task_SupportingEquipmentId",
                table: "Task",
                column: "SupportingEquipmentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activity");

            migrationBuilder.DropTable(
                name: "SessionHistory");

            migrationBuilder.DropTable(
                name: "Task");

            migrationBuilder.DropTable(
                name: "EquipmentShift");

            migrationBuilder.DropTable(
                name: "Location");

            migrationBuilder.DropTable(
                name: "SupportingEquipment");

            migrationBuilder.DropTable(
                name: "Equipment");

            migrationBuilder.DropTable(
                name: "Operator");
        }
    }
}
