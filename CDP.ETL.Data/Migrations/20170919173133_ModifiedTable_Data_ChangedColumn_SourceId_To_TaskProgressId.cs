﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CDP.ETL.Data.Migrations
{
    public partial class ModifiedTable_Data_ChangedColumn_SourceId_To_TaskProgressId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Data_TaskProgress_SourceId",
                table: "Data");

            migrationBuilder.DropIndex(
                name: "IX_TaskProgress_DataId",
                table: "TaskProgress");

            migrationBuilder.RenameColumn(
                name: "SourceId",
                table: "Data",
                newName: "TaskProgressId");

            migrationBuilder.RenameIndex(
                name: "IX_Data_SourceId",
                table: "Data",
                newName: "IX_Data_TaskProgressId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskProgress_DataId",
                table: "TaskProgress",
                column: "DataId");

            migrationBuilder.AddForeignKey(
                name: "FK_Data_TaskProgress_TaskProgressId",
                table: "Data",
                column: "TaskProgressId",
                principalTable: "TaskProgress",
                principalColumn: "TaskProgressId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Data_TaskProgress_TaskProgressId",
                table: "Data");

            migrationBuilder.DropIndex(
                name: "IX_TaskProgress_DataId",
                table: "TaskProgress");

            migrationBuilder.RenameColumn(
                name: "TaskProgressId",
                table: "Data",
                newName: "SourceId");

            migrationBuilder.RenameIndex(
                name: "IX_Data_TaskProgressId",
                table: "Data",
                newName: "IX_Data_SourceId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskProgress_DataId",
                table: "TaskProgress",
                column: "DataId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Data_TaskProgress_SourceId",
                table: "Data",
                column: "SourceId",
                principalTable: "TaskProgress",
                principalColumn: "TaskProgressId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
