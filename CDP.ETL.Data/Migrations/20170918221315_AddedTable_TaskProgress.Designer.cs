﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CDP.ETL.Data.Chug;

namespace CDP.ETL.Data.Migrations
{
    [DbContext(typeof(ChugMobileContext))]
    [Migration("20170918221315_AddedTable_TaskProgress")]
    partial class AddedTable_TaskProgress
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Activity", b =>
                {
                    b.Property<int>("ActivityId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("EquipmentShiftId");

                    b.Property<string>("OperatorId");

                    b.Property<DateTime?>("PerformedAt");

                    b.Property<string>("Reference");

                    b.HasKey("ActivityId");

                    b.HasIndex("EquipmentShiftId");

                    b.HasIndex("OperatorId");

                    b.ToTable("Activity");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Data", b =>
                {
                    b.Property<int>("DataId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Quantity");

                    b.Property<string>("Size");

                    b.Property<string>("SourceId");

                    b.Property<string>("Type");

                    b.HasKey("DataId");

                    b.HasIndex("SourceId");

                    b.ToTable("Data");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Equipment", b =>
                {
                    b.Property<string>("EquipmentId");

                    b.Property<string>("Capacity");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("EquipmentEquipment");

                    b.Property<string>("MachineId");

                    b.Property<string>("Operation");

                    b.Property<string>("OperatorId");

                    b.Property<string>("Status");

                    b.Property<string>("Type");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("EquipmentId");

                    b.HasIndex("OperatorId");

                    b.ToTable("Equipment");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.EquipmentShift", b =>
                {
                    b.Property<string>("EquipmentShiftId")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<decimal?>("DelayUnplannedHours");

                    b.Property<string>("EquipmentId");

                    b.Property<decimal?>("OperationalHours");

                    b.Property<string>("OperatorId");

                    b.Property<string>("Shift");

                    b.Property<DateTime?>("ShiftDate");

                    b.Property<decimal?>("StandbyAutoHours");

                    b.Property<decimal?>("StandbyUnplannedHours");

                    b.Property<string>("Type");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("EquipmentShiftId");

                    b.HasIndex("EquipmentId");

                    b.HasIndex("OperatorId");

                    b.ToTable("EquipmentShift");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Location", b =>
                {
                    b.Property<string>("LocationId");

                    b.Property<string>("LocationHeadingName");

                    b.Property<string>("LocationName");

                    b.Property<string>("Status");

                    b.Property<string>("Type");

                    b.HasKey("LocationId");

                    b.ToTable("Location");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Operator", b =>
                {
                    b.Property<string>("OperatorId");

                    b.Property<string>("Crew");

                    b.Property<string>("FirstName");

                    b.Property<string>("Group");

                    b.Property<string>("LastName");

                    b.Property<string>("Name");

                    b.Property<string>("NickName");

                    b.Property<string>("Role");

                    b.Property<string>("Status");

                    b.Property<string>("Type");

                    b.Property<string>("UserName");

                    b.HasKey("OperatorId");

                    b.ToTable("Operator");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.SessionHistory", b =>
                {
                    b.Property<string>("SessionHistoryId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("AutoEnd");

                    b.Property<string>("AutoEndReason");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("EquipmentId");

                    b.Property<DateTime?>("LogOutTime");

                    b.Property<DateTime?>("LoginTime");

                    b.Property<string>("OperatorId");

                    b.Property<string>("Status");

                    b.Property<string>("Type");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("SessionHistoryId");

                    b.HasIndex("EquipmentId");

                    b.HasIndex("OperatorId");

                    b.ToTable("SessionHistory");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.SupportingEquipment", b =>
                {
                    b.Property<string>("SupportingEquipmentId");

                    b.Property<int?>("Capacity");

                    b.Property<string>("Equipment");

                    b.Property<string>("MachineId");

                    b.Property<string>("Operation");

                    b.Property<string>("Status");

                    b.Property<string>("Type");

                    b.HasKey("SupportingEquipmentId");

                    b.ToTable("SupportingEquipment");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Task", b =>
                {
                    b.Property<string>("TaskId");

                    b.Property<DateTime?>("ActualEndDate");

                    b.Property<int>("ActualQuantity");

                    b.Property<DateTime?>("ActualStartDate");

                    b.Property<bool?>("AutoEnd");

                    b.Property<string>("AutoEndReason");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<string>("Destination");

                    b.Property<string>("EquipmentId");

                    b.Property<bool?>("IsActivityComplete");

                    b.Property<string>("LocationId");

                    b.Property<string>("Material");

                    b.Property<string>("Operation");

                    b.Property<string>("OperatorId");

                    b.Property<string>("OriginalOperation");

                    b.Property<DateTime?>("PlannedEndDate");

                    b.Property<int?>("PlannedQuantity");

                    b.Property<string>("PlannedStartDate");

                    b.Property<int?>("Quantity");

                    b.Property<string>("Shift");

                    b.Property<DateTime?>("ShiftDate");

                    b.Property<string>("Status");

                    b.Property<string>("SupervisorId");

                    b.Property<string>("SupportingEquipmentId");

                    b.Property<string>("SupportingOperation");

                    b.Property<string>("Type");

                    b.Property<DateTime?>("UpdatedAt");

                    b.HasKey("TaskId");

                    b.HasIndex("EquipmentId");

                    b.HasIndex("LocationId");

                    b.HasIndex("OperatorId");

                    b.HasIndex("SupervisorId");

                    b.HasIndex("SupportingEquipmentId");

                    b.ToTable("Task");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.TaskProgress", b =>
                {
                    b.Property<string>("TaskProgressId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<DateTime?>("CreatedAt");

                    b.Property<int?>("DataId");

                    b.Property<string>("EquipmentId");

                    b.Property<string>("LocationId");

                    b.Property<string>("OperatorId");

                    b.Property<string>("TaskId");

                    b.Property<string>("Type");

                    b.Property<DateTime?>("UpdateAt");

                    b.HasKey("TaskProgressId");

                    b.HasIndex("DataId")
                        .IsUnique();

                    b.HasIndex("EquipmentId");

                    b.HasIndex("LocationId");

                    b.HasIndex("OperatorId");

                    b.HasIndex("TaskId");

                    b.ToTable("TaskProgress");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Activity", b =>
                {
                    b.HasOne("CDP.ETL.Data.Chug.Entities.EquipmentShift", "EquipmentShift")
                        .WithMany("Activities")
                        .HasForeignKey("EquipmentShiftId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Operator", "Owner")
                        .WithMany()
                        .HasForeignKey("OperatorId");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Data", b =>
                {
                    b.HasOne("CDP.ETL.Data.Chug.Entities.TaskProgress", "TaskProgress")
                        .WithMany()
                        .HasForeignKey("SourceId");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Equipment", b =>
                {
                    b.HasOne("CDP.ETL.Data.Chug.Entities.Operator", "Operator")
                        .WithMany()
                        .HasForeignKey("OperatorId");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.EquipmentShift", b =>
                {
                    b.HasOne("CDP.ETL.Data.Chug.Entities.Equipment", "Equipment")
                        .WithMany()
                        .HasForeignKey("EquipmentId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Operator", "Owner")
                        .WithMany()
                        .HasForeignKey("OperatorId");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.SessionHistory", b =>
                {
                    b.HasOne("CDP.ETL.Data.Chug.Entities.Equipment", "Equipment")
                        .WithMany()
                        .HasForeignKey("EquipmentId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Operator", "Owner")
                        .WithMany()
                        .HasForeignKey("OperatorId");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.Task", b =>
                {
                    b.HasOne("CDP.ETL.Data.Chug.Entities.Equipment", "Equipment")
                        .WithMany()
                        .HasForeignKey("EquipmentId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Location", "Location")
                        .WithMany()
                        .HasForeignKey("LocationId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Operator", "Operator")
                        .WithMany()
                        .HasForeignKey("OperatorId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Operator", "Supervisor")
                        .WithMany()
                        .HasForeignKey("SupervisorId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.SupportingEquipment", "SupportingEquipment")
                        .WithMany("Tasks")
                        .HasForeignKey("SupportingEquipmentId");
                });

            modelBuilder.Entity("CDP.ETL.Data.Chug.Entities.TaskProgress", b =>
                {
                    b.HasOne("CDP.ETL.Data.Chug.Entities.Data", "Data")
                        .WithOne()
                        .HasForeignKey("CDP.ETL.Data.Chug.Entities.TaskProgress", "DataId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Equipment", "Equipment")
                        .WithMany()
                        .HasForeignKey("EquipmentId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Location", "Location")
                        .WithMany()
                        .HasForeignKey("LocationId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Operator", "Owner")
                        .WithMany()
                        .HasForeignKey("OperatorId");

                    b.HasOne("CDP.ETL.Data.Chug.Entities.Task", "Task")
                        .WithMany()
                        .HasForeignKey("TaskId");
                });
        }
    }
}
