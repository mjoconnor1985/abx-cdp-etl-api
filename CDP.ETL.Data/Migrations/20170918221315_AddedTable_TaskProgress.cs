﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace CDP.ETL.Data.Migrations
{
    public partial class AddedTable_TaskProgress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Location_Operator_OperatorId",
                table: "Location");

            migrationBuilder.DropIndex(
                name: "IX_Location_OperatorId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "OperatorId",
                table: "Location");

            migrationBuilder.DropColumn(
                name: "UpdatedAt",
                table: "Location");

            migrationBuilder.CreateTable(
                name: "TaskProgress",
                columns: table => new
                {
                    TaskProgressId = table.Column<string>(nullable: false),
                    Action = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: true),
                    DataId = table.Column<int>(nullable: true),
                    EquipmentId = table.Column<string>(nullable: true),
                    LocationId = table.Column<string>(nullable: true),
                    OperatorId = table.Column<string>(nullable: true),
                    TaskId = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UpdateAt = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskProgress", x => x.TaskProgressId);
                    table.ForeignKey(
                        name: "FK_TaskProgress_Equipment_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "Equipment",
                        principalColumn: "EquipmentId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskProgress_Location_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Location",
                        principalColumn: "LocationId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskProgress_Operator_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operator",
                        principalColumn: "OperatorId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaskProgress_Task_TaskId",
                        column: x => x.TaskId,
                        principalTable: "Task",
                        principalColumn: "TaskId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Data",
                columns: table => new
                {
                    DataId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Quantity = table.Column<int>(nullable: false),
                    Size = table.Column<string>(nullable: true),
                    SourceId = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Data", x => x.DataId);
                    table.ForeignKey(
                        name: "FK_Data_TaskProgress_SourceId",
                        column: x => x.SourceId,
                        principalTable: "TaskProgress",
                        principalColumn: "TaskProgressId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Data_SourceId",
                table: "Data",
                column: "SourceId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskProgress_DataId",
                table: "TaskProgress",
                column: "DataId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_TaskProgress_EquipmentId",
                table: "TaskProgress",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskProgress_LocationId",
                table: "TaskProgress",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskProgress_OperatorId",
                table: "TaskProgress",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_TaskProgress_TaskId",
                table: "TaskProgress",
                column: "TaskId");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskProgress_Data_DataId",
                table: "TaskProgress",
                column: "DataId",
                principalTable: "Data",
                principalColumn: "DataId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Data_TaskProgress_SourceId",
                table: "Data");

            migrationBuilder.DropTable(
                name: "TaskProgress");

            migrationBuilder.DropTable(
                name: "Data");

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OperatorId",
                table: "Location",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdatedAt",
                table: "Location",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Location_OperatorId",
                table: "Location",
                column: "OperatorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Location_Operator_OperatorId",
                table: "Location",
                column: "OperatorId",
                principalTable: "Operator",
                principalColumn: "OperatorId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
