﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using RestEase;
using Newtonsoft.Json.Linq;
using CDP.ETL.Data.RBA.Models;
using CDP.ETL.Data.RBA;

namespace CDP.ETL.Data
{
    public class RBADataSource : IRBADataSource
    {
        #region Fields

        IConfiguration _config;

        #endregion

        #region Constructors

        public RBADataSource(IConfiguration config)
        {
            _config = config;
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<Consumable>> GetConsumablesAsync(DateTime date)
        {
            var rbaApi = RestClient.For<IRbaApi>(_config["ApiEndpoints:Rba"]);

            var data = await rbaApi.GetRbaDataAsync(date.Date);

            var consumables = new List<Consumable>();

            foreach (var d in data)
            {
                JObject jo = (JObject)d;
                if (jo["type"] != null && jo["type"].Type != JTokenType.Null && jo["type"].ToString() == "_gh_consumables")
                    consumables.Add(d.ToObject<Consumable>());
            }

            return consumables;
        }

        public async Task<RbaDataDump> GetRbaDataAsync(DateTime date)
        {
            var rbaApi = RestClient.For<IRbaApi>(_config["ApiEndpoints:Rba"]);

            var rbaData = await rbaApi.GetRbaDataAsync(date.Date);
            
            return ProsessRbaData(rbaData);
        }
        
        public RbaDataDump ProsessRbaData(dynamic rawData)
        {
            var rbaDataDump = new RbaDataDump();

            foreach (var data in rawData)
            {
                JObject jObject = (JObject)data;
                
               if (jObject["type"] != null && jObject["type"].Type != JTokenType.Null && jObject["type"].ToString() == "_gh_task")
                {
                    rbaDataDump.Tasks.Add(data.ToObject<RBA.Models.Task>());
                }

                else if (jObject["type"] != null && jObject["type"].Type != JTokenType.Null && jObject["type"].ToString() == "_gh_session_history")
                {
                    rbaDataDump.SessionHistory.Add(data.ToObject<SessionHistory>());
                }

                else if (jObject["type"] != null && jObject["type"].Type != JTokenType.Null && jObject["type"].ToString() == "_gh_equipment_shift")
                {
                    rbaDataDump.EquipmentShifts.Add(data.ToObject<EquipmentShift>());
                }

                else if (jObject["type"] != null && jObject["type"].Type != JTokenType.Null && jObject["type"].ToString() == "_gh_task_progress")
                {
                    rbaDataDump.TaskProgress.Add(data.ToObject<TaskProgress>());
                }

                else
                    continue;
            }

            return rbaDataDump;
        }
        
        #endregion
    }
}
