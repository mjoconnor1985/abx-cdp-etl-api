﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RestEase;
using CDP.ETL.Data.RBA.Models;

namespace CDP.ETL.Data.RBA
{
    public interface IRbaApi
    {
        [Get("api/utils/data/{date}")]
        Task<IEnumerable<dynamic>> GetRbaDataAsync([Path] DateTime date);
    }
}
