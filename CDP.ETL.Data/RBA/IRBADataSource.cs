﻿using CDP.ETL.Data.RBA.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CDP.ETL.Data
{
    public interface IRBADataSource
    {
        Task<IEnumerable<Consumable>> GetConsumablesAsync(DateTime date);

        Task<RbaDataDump> GetRbaDataAsync(DateTime date);

        RbaDataDump ProsessRbaData(dynamic rawData);
    }
}
