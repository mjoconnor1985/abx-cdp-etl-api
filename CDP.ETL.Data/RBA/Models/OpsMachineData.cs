﻿using System.Collections.Generic;

namespace CDP.ETL.Data.RBA.Models
{
    public class OpsMachineData
    {
        public IEnumerable<Answer> Answers { get; set; }

        public string Group { get; set; }

        public string Question { get; set; }
    }
}