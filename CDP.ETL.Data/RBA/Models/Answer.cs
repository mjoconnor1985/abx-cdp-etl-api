﻿namespace CDP.ETL.Data.RBA.Models
{
    public class Answer
    {
        public string Comment { get; set; }

        public string CreatedAt { get; set; }

        public string UpdatedAt { get; set; }

        public string Value { get; set; }
    }
}