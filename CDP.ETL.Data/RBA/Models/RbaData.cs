﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class RbaData
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        public DateTime? CreatedAt { get; set; }
        
        public Owner Owner { get; set; }

        public string Type { get; set; }

        public DateTime? UpdatedAt { get; set; }
    }
}
