﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class OpsLocations: RbaData
    {
        public int CrCount { get; set; }

        public IEnumerable<Data> Data { get; set; }

        public Equipment Equpment { get; set; }

        public Location Location { get; set; }

        public string Notes { get; set; }

        public int NrCout { get; set; }

        public int OkCount { get; set; }

        public string OpsType { get; set; }

        public object Review { get; set; }

        public TaskChild Task { get; set; }
    }
}
