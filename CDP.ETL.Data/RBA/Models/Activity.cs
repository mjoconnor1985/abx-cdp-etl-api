﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Activity
    {
        public string Action { get; set; }

        public DateTime? CreatedAt { get; set; }

        public Owner Owner { get; set; }

        public DateTime? PerformedAt { get; set; }

        public string Reference { get; set; }
    }
}
