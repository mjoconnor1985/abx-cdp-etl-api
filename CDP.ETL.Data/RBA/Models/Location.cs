﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Location
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        [JsonProperty("Location")]
        public string LocationHeadingName { get; set; }

        public string LocationName { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }
    }
}
