﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Task: RbaData
    {
        public DateTime? ActualEndDate { get; set; }

        public string ActualQuantity { get; set; }

        public DateTime? ActualStartDate { get; set; }

        public bool? AutoEnd { get; set; }

        public string AutoEndReason { get; set; }

        public string Destination { get; set; }

        public Equipment Equipment { get; set; }

        public bool? IsActivityComplete { get; set; }

        public Location LocationChild { get; set; }

        public TaskMaintainanceReason MaintainenceReason { get; set; }

        public string Material { get; set; }

        public string Operation { get; set; }

        public Owner Operator { get; set; }

        public string OriginalOperation { get; set; }

        public DateTime? PlannedEndDate { get; set; }

        public string PlannedQuantity { get; set; }

        public DateTime? PlannedStartDate { get; set; }

        public int? Quantity { get; set; }

        public string Shift { get; set; }

        public DateTime? ShiftDate { get; set; }

        public string Status { get; set; }

        public SupportingEquipment SupportingEquipment { get; set; }

        public string SupportingOperation { get; set; }

    }
}
