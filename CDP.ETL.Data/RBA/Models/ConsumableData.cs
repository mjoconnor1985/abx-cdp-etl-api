﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class ConsumableData
    {
        public string Quantity { get; set; }

        public string Type { get; set; }
    }
}
