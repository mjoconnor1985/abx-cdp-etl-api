﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class ConsumableEquipment
    {
        [JsonProperty("_id")]
        public string ConsumableEquipmentId { get; set; }

        public string Equipment { get; set; }

        public string Operation { get; set; }

        public string Type { get; set; }
    }
}
