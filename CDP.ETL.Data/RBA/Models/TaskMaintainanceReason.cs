﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class TaskMaintainanceReason
    {
        public Owner Operator { get; set; }

        public Supervisor Supervisor { get; set; }
    }
}
