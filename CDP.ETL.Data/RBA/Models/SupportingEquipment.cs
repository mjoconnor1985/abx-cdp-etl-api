﻿using Newtonsoft.Json;

namespace CDP.ETL.Data.RBA.Models
{
    public class SupportingEquipment
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        public int Capacity { get; set; }

        public string Equipment { get; set; }

        public string MachineId { get; set; }

        public string Operation { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }
    }
}