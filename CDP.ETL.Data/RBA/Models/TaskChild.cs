﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class TaskChild
    {
        [JsonProperty("_id")]
        public string TaskId { get; set; }

        public string Operation { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }
    }
}
