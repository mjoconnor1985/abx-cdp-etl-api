﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Supervisor
    {
        public string CategoryCode { get; set; }

        public int Code { get; set; }

        public string Description { get; set; }

        public string Displaycode { get; set; }

        public Comment Comments { get; set; }
    }
}
