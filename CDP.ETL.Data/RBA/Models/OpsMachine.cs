﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class OpsMachine
    {
        public IEnumerable<Data> Data { get; set; }

        public Equipment Equipment { get; set; }

        public string MachineHours { get; set; }

        public string Notes { get; set; }

        public int NrCount { get; set; }

        public int OkCount { get; set; }

        public string OpsType { get; set; }

        public Object Review { get; set; }

        public string Status { get; set; }
    }
}
