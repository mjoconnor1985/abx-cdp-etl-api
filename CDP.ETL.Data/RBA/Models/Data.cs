﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Data
    {
        public string Quantity { get; set; }

        public string Size { get; set; }

        public string Type { get; set; }
    }
}
