﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Owner
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        public string Crew { get; set; }

        public string FirstName { get; set; }

        public string Group { get; set; }

        public string LastName { get; set; }

        public string Name { get; set; }

        public string NickName { get; set; }

        public string Role { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public string UserName { get; set; }
    }
}
