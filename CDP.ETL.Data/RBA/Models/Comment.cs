﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Comment
    {
        public string Operator { get; set; }

        public string Supervisor { get; set; }
    }
}
