﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Value
    {
        public string CategoryCode { get; set; }

        public string Description { get; set; }
    }
}
