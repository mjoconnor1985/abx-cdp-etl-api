﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class SessionHistory: RbaData
    {
        public bool? AutoEnd { get; set; }

        public string AutoEndReason { get; set; }

        public Equipment Equipment { get; set; }

        public DateTime? LoginTime { get; set; }

        public DateTime? LogOutTime { get; set; }

        public string Status { get; set; }
    }
}
