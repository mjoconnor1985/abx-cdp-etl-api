﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Equipment
    {
        [JsonProperty("_id")]
        public string Id { get; set; }

        public int? Capacity { get; set; }

        [JsonProperty("equipment")]
        public string EquipmentEquipment { get; set; }

        public string MachineId { get; set; }

        public string Operation { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }
    }
}
