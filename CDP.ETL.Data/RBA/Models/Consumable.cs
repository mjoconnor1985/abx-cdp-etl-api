﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;

namespace CDP.ETL.Data.RBA.Models
{
    public class Consumable: RbaData
    {
        public List<ConsumableData> Data { get; set; }

        public string Notes { get; set; }
        
        public TaskChild Task { get; set; }

        [JsonProperty("equipment")]
        public ConsumableEquipment ConsumableEquipment { get; set; }
    }
}
