﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class EquipmentShift: RbaData
    {
        public List<Activity> Activities { get; set; }

        public Equipment Equipment { get; set; }
        
        public Fields Fields { get; set; }

        public string Shift { get; set; }

        public DateTime? ShiftDate { get; set; }
    }
}
