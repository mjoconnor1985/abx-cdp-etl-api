﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class TaskProgress: RbaData
    {
        public string Action { get; set; }

        public string Cycle { get; set; }

        public Data Data { get; set; }

        public Equipment Equipment { get; set; }

        public Location Location { get; set; }

        public TaskChild Task { get; set; }
    }
}
