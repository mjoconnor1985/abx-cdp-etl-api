﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class EquipmentUntilStatus: RbaData
    {
        public Equipment Equipment { get; set; }

        public MaintainanceReason MaintainanceReason { get; set; }

        public string Notes { get; set; }

        public string Status { get; set; }

        public TaskChild Task { get; set; }

        public Value Value { get; set; }
    }
}
