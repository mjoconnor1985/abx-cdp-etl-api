﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class RbaDataDump
    {
        public RbaDataDump()
        {
            Tasks = new List<Task>();
            Operators = new List<Owner>();
            RelatedEquipment = new List<Equipment>();
            RelatedLocations = new List<Location>();
            SupportingEquipment = new List<SupportingEquipment>();
            SessionHistory = new List<Models.SessionHistory>();
            EquipmentShifts = new List<EquipmentShift>();
            TaskProgress = new List<Models.TaskProgress>();
        }

        public List<Task> Tasks { get; set; }

        public List<Owner> Operators { get; set; }

        public List<Equipment> RelatedEquipment { get; set; }

        public List<Location> RelatedLocations { get; set; }

        public List<SupportingEquipment> SupportingEquipment { get; set; }

        public List<SessionHistory> SessionHistory { get; set; }

        public List<EquipmentShift> EquipmentShifts { get; set; }

        public List<TaskProgress> TaskProgress { get; set; }
    }
}
