﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.RBA.Models
{
    public class Fields
    {
        public Delay Delay { get; set; }
        public decimal? OpHours { get; set; }
        public Standby Standby { get; set; }
    }
    public class Delay
    {
        public decimal? Unplanned { get; set; }
    }

    public class Standby
    {
        public decimal? Auto { get; set; }
        public decimal? Unplanned { get; set; }
    }

  
}
