﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using api = CDP.ETL.Data.RBA.Models;
using entities = CDP.ETL.Data.Chug.Entities;

namespace CDP.ETL.Data.MappingProfiles
{
    public class ConsumableProfile : Profile
    {
        public ConsumableProfile()
        {
            //RBA API to Chug Db
            CreateMap<api.Consumable, entities.Consumable>()
                .ForMember(dest => dest.ConsumableId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Operator, opt => opt.MapFrom(src => src.Owner))
                 .ForMember(dest => dest.ConsumableData, opt => opt.MapFrom(src => src.Data))
                 .ForMember(dest => dest.SubTask, opt => opt.MapFrom(src => src.Task));

            CreateMap<api.ConsumableData, entities.ConsumableData>()
                .ForMember(dest => dest.Quantity, opt => opt.ResolveUsing(src =>
                {
                    return int.TryParse(src.Quantity, out var quantity) ? quantity : (int?)null;
                }));

            CreateMap<api.Owner, entities.Operator>()
                .ForMember(dest => dest.OperatorId, opt => opt.MapFrom(src => src.Id));

            CreateMap<api.TaskChild, entities.SubTask>()
                .ForMember(dest => dest.SubTaskId, opt => opt.MapFrom(src => src.TaskId));

            CreateMap<api.ConsumableEquipment, entities.ConsumableEquipment>();
        }
    }
}
