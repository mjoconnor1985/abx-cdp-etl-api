﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using entities = CDP.ETL.Data.Chug.Entities;
using api = CDP.ETL.Data.RBA.Models;

namespace CDP.ETL.Data.MappingProfiles
{
    public class TaskProgressProfile: Profile
    {
        public TaskProgressProfile()
        {
            CreateMap<api.TaskProgress, entities.TaskProgress>()
                .ForMember(dest => dest.TaskProgressId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.EquipmentId, opt => opt.ResolveUsing(src => src?.Equipment.Id))
                .ForMember(dest => dest.LocationId, opt => opt.ResolveUsing(src => src?.Location.Id))
                .ForMember(dest => dest.TaskId, opt => opt.ResolveUsing(src => src?.Task.TaskId))
                .ForMember(dest => dest.OperatorId, opt => opt.ResolveUsing(src => src?.Owner.Id));

            CreateMap<api.Data, entities.Data>();
        }
    }
}
