﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using entities = CDP.ETL.Data.Chug.Entities;
using api = CDP.ETL.Data.RBA.Models;

namespace CDP.ETL.Data.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<api.Task, entities.Task>()
                .ForMember(dest => dest.TaskId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.LocationChild))
                .ForMember(dest => dest.SupervisorId, opt => opt.ResolveUsing(src =>
                {
                    return src.Owner != null ? src.Owner.Id : null;
                }))
                .ForMember(dest => dest.ActualQuantity, opt => opt.ResolveUsing(src =>
                 {
                     return int.TryParse(src.ActualQuantity, out var quantity) ? quantity : 0;
                 }))
                .ForMember(dest => dest.PlannedQuantity, opt => opt.ResolveUsing(src =>
                 {
                     return int.TryParse(src.PlannedQuantity, out var quantity) ? quantity : 0;
                 }));

            CreateMap<api.TaskChild, entities.Task>();

            CreateMap<api.Equipment, entities.Equipment>()
                .ForMember(dest => dest.EquipmentId, opt => opt.MapFrom(src => src.Id));

            CreateMap<api.Location, entities.Location>()
                .ForMember(dest => dest.LocationId, opt => opt.MapFrom(src => src.Id));

            CreateMap<api.SupportingEquipment, entities.SupportingEquipment>()
                .ForMember(dest => dest.SupportingEquipmentId, opt => opt.MapFrom(src => src.Id));
        }
    }
}
