﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using api = CDP.ETL.Data.RBA.Models;
using data = CDP.ETL.Data.Chug.Entities;

namespace CDP.ETL.Data.MappingProfiles
{
    public class SessionHistoryProfile : Profile
    {
        public SessionHistoryProfile()
        {
            CreateMap<api.SessionHistory, data.SessionHistory>()
                .ForMember(dest => dest.SessionHistoryId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.OperatorId, opt => opt.ResolveUsing(src =>
                {
                    return src.Owner != null ? src.Owner.Id : null;
                }))
                 .ForMember(dest => dest.EquipmentId, opt => opt.ResolveUsing(src =>
                 {
                     return src.Equipment != null ? src.Equipment.Id : null;
                 }));
        }
    }
}
