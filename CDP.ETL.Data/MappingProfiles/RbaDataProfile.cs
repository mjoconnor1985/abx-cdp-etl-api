﻿using AutoMapper;
using CDP.ETL.Data.RBA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CDP.ETL.Data.MappingProfiles
{
    public class RbaDataProfile: Profile
    {
        public RbaDataProfile()
        {
            CreateMap<RbaDataDump, ETL.Data.Chug.RbaData>();
        }
    }
}
