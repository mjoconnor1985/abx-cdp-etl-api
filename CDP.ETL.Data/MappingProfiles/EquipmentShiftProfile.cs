﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using api = CDP.ETL.Data.RBA.Models;
using entities = CDP.ETL.Data.Chug.Entities;

namespace CDP.ETL.Data.MappingProfiles
{
    public class EquipmentShiftProfile : Profile
    {
        public EquipmentShiftProfile()
        {
            CreateMap<api.EquipmentShift, entities.EquipmentShift>()
                .ForMember(dest => dest.EquipmentShiftId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.OperationalHours, opt => opt.ResolveUsing(src =>
                {
                    return src.Fields?.OpHours != null ? src.Fields.OpHours : (decimal?)null;
                }))
                .ForMember(dest => dest.StandbyAutoHours, opt => opt.ResolveUsing(src =>
                {
                    return src.Fields?.Standby?.Auto != null ? src.Fields.Standby.Auto : (decimal?)null;
                }))
                 .ForMember(dest => dest.StandbyUnplannedHours, opt => opt.ResolveUsing(src =>
                 {
                     return src.Fields?.Standby?.Unplanned != null ? src.Fields.Standby.Unplanned : (decimal?)null;
                 }))
                 .ForMember(dest => dest.DelayUnplannedHours, opt => opt.ResolveUsing(src =>
                 {
                     return src.Fields?.Delay?.Unplanned != null ? src.Fields.Delay.Unplanned : (decimal?)null;
                 }));
            CreateMap<api.Activity, entities.Activity>();
        }
    }
}
