﻿using CDP.ETL.Data.Chug.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.Chug
{
    public class RbaData
    {
        public List<Task> Tasks { get; set; }

        public List<Operator> Operators { get; set; }

        public List<Equipment> RelatedEquipment { get; set; }

        public List<Location> RelatedLocations { get; set; }

        public List<SupportingEquipment> SupportingEquipment { get; set; }

        public List<SessionHistory> SessionHistory { get; set; }

        public List<EquipmentShift> EquipmentShifts { get; set; }

        public List<TaskProgress> TaskProgress { get; set; }
    }
}
