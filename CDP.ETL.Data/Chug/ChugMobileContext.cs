﻿using CDP.ETL.Data.Chug.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.Chug
{
    public class ChugMobileContext: DbContext
    {
        public ChugMobileContext(DbContextOptions options) : base(options) { }
        
        public virtual DbSet<Task> Task { get; set; }

        public virtual DbSet<Operator> Operator { get; set; }
        
        public virtual DbSet<Location> Location { get; set; }

        public virtual DbSet<Equipment> Equipment { get; set; }

        public virtual DbSet<SupportingEquipment> SupportingEquipment { get; set; }

        public virtual DbSet<SessionHistory> SessionHistory { get; set; }

        public virtual DbSet<EquipmentShift> EquipmentShift { get; set; }

        public virtual DbSet<TaskProgress> TaskProgress { get; set; }

        public virtual DbSet<Entities.Data> Data { get; set; }
    }
}
