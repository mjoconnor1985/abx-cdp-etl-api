﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class TaskProgress
    {
        public string TaskProgressId { get; set; }

        public int? DataId { get; set; }

        public string EquipmentId { get; set; }

        public string LocationId { get; set; }

        public string TaskId { get; set; }

        public string OperatorId { get; set; }

        public string Action { get; set; }

        public DateTime? CreatedAt { get; set; }

        public string Type { get; set; }
        
        public DateTime? UpdatedAt { get; set; }
        
        [ForeignKey("DataId")]
        public Data Data { get; set; }

        public Equipment Equipment { get; set; }

        public Location Location { get; set; }

        [ForeignKey("OperatorId")]
        public Operator Owner { get; set; }

        public Task Task { get; set; }
        
    }
}
