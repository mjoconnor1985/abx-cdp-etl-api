﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDP.ETL.Data.Chug.Entities
{
    public class SupportingEquipment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SupportingEquipmentId { get; set; }

        public int? Capacity { get; set; }

        public string Equipment { get; set; }

        public string MachineId { get; set; }

        public string Operation { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public List<Task> Tasks { get;  set; }
    }
}