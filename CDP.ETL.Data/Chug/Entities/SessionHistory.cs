﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class SessionHistory
    {
        public string SessionHistoryId { get; set; }

        public string OperatorId { get; set; }

        public string EquipmentId { get; set; }

        public bool? AutoEnd { get; set; }

        public string AutoEndReason { get; set; }

        public DateTime? CreatedAt { get; set; }
        
        public DateTime? LoginTime { get; set; }

        public DateTime? LogOutTime { get; set; }
        
        public string Status { get; set; }

        public string Type { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public Equipment Equipment { get; set; }

        public Operator Owner { get; set; }
    }
}
