﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class EquipmentShift
    {
        public string EquipmentShiftId { get; set; }

        public string EquipmentId { get; set; }

        public string OperatorId { get; set; }

        public DateTime? CreatedAt { get; set; }

        public Equipment Equipment { get; set; }

        public decimal? OperationalHours { get; set; }

        public decimal? StandbyAutoHours { get; set; }

        public decimal? StandbyUnplannedHours { get; set; }

        public decimal? DelayUnplannedHours { get; set; }

        public string Shift { get; set; }

        public DateTime? ShiftDate { get; set; }

        public Operator Owner { get; set; }

        public string Type { get; set; }

        public DateTime? UpdatedAt { get; set; }
        
        public List<Activity> Activities { get; set; }
    }
}
