﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDP.ETL.Data.Chug.Entities
{
    public class Operator
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string OperatorId { get; set; }

        public string Crew { get; set; }

        public string FirstName { get; set; }

        public string Group { get; set; }

        public string LastName { get; set; }

        public string Name { get; set; }

        public string NickName { get; set; }

        public string Role { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }

        public string UserName { get; set; }
    }
}