﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CDP.ETL.Data.Chug.Entities
{
    public class ConsumableData
    {
        public int ConsumableDataId { get; set; }

        public string ConsumableId { get; set; }

        public int? Quantity { get; set; }

        public string Type { get; set; }

        public Consumable Consumable {get; set;}

    }
}