﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class Task
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string TaskId { get; set; }

        public string EquipmentId { get; set; }

        public string LocationId { get; set; }
        
        public string OperatorId { get; set; }

        public string SupervisorId { get; set; }

        public string SupportingEquipmentId { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? ActualEndDate { get; set; }

        public int ActualQuantity { get; set; }

        public DateTime? ActualStartDate { get; set; }

        public bool? AutoEnd { get; set; }

        public string AutoEndReason { get; set; }

        public string Destination { get; set; }

        public bool? IsActivityComplete { get; set; }

        public string Material { get; set; }

        public string Operation { get; set; }

        public string OriginalOperation { get; set; }

        public DateTime? PlannedEndDate { get; set; }

        public int? PlannedQuantity { get; set; }

        public string PlannedStartDate { get; set; }

        public int? Quantity { get; set; }

        public string Shift { get; set; }

        public DateTime? ShiftDate { get; set; }

        public string Status { get; set; }

        public string SupportingOperation { get; set; }

        public string Type { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public Equipment Equipment { get; set; }

        public Location Location { get; set; }

        [ForeignKey("OperatorId")]
        public Operator Operator { get; set; }

        [ForeignKey("SupervisorId")]
        public Operator Supervisor { get; set; }

        public SupportingEquipment SupportingEquipment { get; set; }
    }
}
