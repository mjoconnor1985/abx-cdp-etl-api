﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class Consumable
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ConsumableId { get; set; }

        public string OperatorId { get; set; }

        public string SubTaskId { get; set; }

        public string ConsumableEquipmentId { get; set; }

        public DateTime? CreatedAt { get; set; }

        public string Notes { get; set; }

        public string Type { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public ICollection<ConsumableData> ConsumableData { get; set; }

        public Operator Operator { get; set;}

        public SubTask SubTask { get; set; }

        public ConsumableEquipment ConsumableEquipment { get; set; }


    }
}
