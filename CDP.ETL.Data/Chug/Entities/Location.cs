﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class Location
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string LocationId { get; set; }

        public string LocationHeadingName { get; set; }

        public string LocationName { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }
    }
}
