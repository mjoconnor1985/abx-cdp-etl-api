﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CDP.ETL.Data.Chug.Entities
{
    public class SubTask
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SubTaskId { get; set;}

        public string Operation { get; set; }

        public string Status { get; set; }

        public string Type { get; set; }
    }
}