﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class Equipment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string EquipmentId { get; set; }

        public string OperatorId { get; set; }

        public string Capacity { get; set; }

        public DateTime? CreatedAt { get; set; }

        public string EquipmentEquipment { get; set; }

        public string MachineId { get; set; }

        public string Operation { get; set; }

        public string Type { get; set; }

        public string Status { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public Operator Operator { get; set; }

        List<Task> Tasks { get; set;  }
    }
}
