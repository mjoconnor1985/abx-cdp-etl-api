﻿using CDP.ETL.Data.RBA.Models;
using System;

namespace CDP.ETL.Data.Chug.Entities
{
    public class Activity
    {
        public int ActivityId { get; set; }

        public string EquipmentShiftId { get; set; }

        public string OperatorId { get; set; }

        public string Action { get; set; }

        public DateTime? CreatedAt { get; set; }
        
        public DateTime? PerformedAt { get; set; }

        public string Reference { get; set; }

        public EquipmentShift EquipmentShift { get; set; }
        
        public Operator Owner { get; set; }
    }
}