﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class Data
    {
        public int DataId { get; set; }

        public string TaskProgressId { get; set; }

        public int Quantity { get; set; }

        public string Size { get; set; }

        public string Type { get; set; }

        [ForeignKey("TaskProgressId")]
        public TaskProgress TaskProgress { get; set; }
    }
}
