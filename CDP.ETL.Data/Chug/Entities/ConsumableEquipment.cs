﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CDP.ETL.Data.Chug.Entities
{
    public class ConsumableEquipment
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ConsumableEquipmentId { get; set; }

        public string Equipment { get; set; }

        public string Operation { get; set; }

        public string Type { get; set; }
    }
}
