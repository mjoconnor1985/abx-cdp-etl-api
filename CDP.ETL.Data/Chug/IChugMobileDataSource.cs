﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CDP.ETL.Data.Chug
{
    public interface IChugMobileDataSource
    {
        Task<int> SaveRbaData(RbaData rbaData);
    }
}
