﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CDP.ETL.Data.Chug.Entities;

namespace CDP.ETL.Data.Chug
{
    public class ChugMobileDataSource : IChugMobileDataSource
    {
        #region Fields

        IMapper _mapper;
        ChugMobileContext _chugDb;

        #endregion

        #region Constructors


        public ChugMobileDataSource(IMapper mapper, ChugMobileContext dbContext)
        {
            _chugDb = dbContext;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        #region Public 

        public async Task<int> SaveRbaData(RbaData rbaData)
        {
            var rowsSaved = 0;
            var cleanRbaData = FlattenData(rbaData);

            //First save the child objects
            _chugDb.Operator.AddRange(cleanRbaData.Operators);
            _chugDb.Equipment.AddRange(cleanRbaData.RelatedEquipment);
            _chugDb.Location.AddRange(cleanRbaData.RelatedLocations);
            _chugDb.SupportingEquipment.AddRange(cleanRbaData.SupportingEquipment);

            //Get existing records from the db
            var operatorIds = cleanRbaData.Operators.Select(c => c.OperatorId);
            var relatedEquipmentIds = cleanRbaData.RelatedEquipment.Select(t => t.EquipmentId);
            var relatedLocationIds = cleanRbaData.RelatedLocations.Select(s => s.LocationId);
            var supportingEquipmentIds = cleanRbaData.SupportingEquipment.Select(s => s.SupportingEquipmentId);

            var existingOperators = await _chugDb.Operator
                .Where(o => operatorIds.Contains(o.OperatorId))
                .ToListAsync();
            var existingRelatedEquipment = await _chugDb.Equipment
                .Where(w => relatedEquipmentIds.Contains(w.EquipmentId))
                .ToListAsync();
            var existingRelatedLocations = await _chugDb.Location
                .Where(t => relatedLocationIds.Contains(t.LocationId))
                .ToListAsync();
            var existingSupportingEquipment = await _chugDb.SupportingEquipment
               .Where(w => supportingEquipmentIds.Contains(w.SupportingEquipmentId))
               .ToListAsync();

            //If the record exists change state to modified so that it will update and not insert.
            foreach (var op in existingOperators)
            {
                _chugDb.Entry(op).State = EntityState.Modified;
            }

            foreach (var equipment in existingRelatedEquipment)
            {
                _chugDb.Entry(equipment).State = EntityState.Modified;
            }

            foreach (var location in existingRelatedLocations)
            {
                _chugDb.Entry(location).State = EntityState.Modified;
            }

            foreach (var equipment in existingSupportingEquipment)
            {
                _chugDb.Entry(equipment).State = EntityState.Modified;
            }

            rowsSaved = await _chugDb.SaveChangesAsync();

            //Now add the parrent objects
            _chugDb.Task.AddRange(cleanRbaData.Tasks);
            _chugDb.SessionHistory.AddRange(cleanRbaData.SessionHistory);
            _chugDb.EquipmentShift.AddRange(cleanRbaData.EquipmentShifts);
            _chugDb.TaskProgress.AddRange(cleanRbaData.TaskProgress);

            //Get existing records from the db
            var taskIds = cleanRbaData.Tasks.Select(t => t.TaskId);
            var sessionHistoryIds = cleanRbaData.SessionHistory.Select(s => s.SessionHistoryId);
            var equipmentShiftIds = cleanRbaData.EquipmentShifts.Select(s => s.EquipmentShiftId);
            var existingTaskProgressIds = cleanRbaData.TaskProgress.Select(s => s.TaskProgressId);

            var existingTasks = await _chugDb.Task
                .Where(t => taskIds.Contains(t.TaskId))
                .ToListAsync();
            var existingSessionHistory = await _chugDb.SessionHistory
                .Where(w => sessionHistoryIds.Contains(w.SessionHistoryId))
                .ToListAsync();
            var existingEquipmentShifts = await _chugDb.EquipmentShift
                .Where(w => equipmentShiftIds.Contains(w.EquipmentShiftId))
                .ToListAsync();
            var exisitngTaskProgress = await _chugDb.TaskProgress
                .Where(w => existingTaskProgressIds.Contains(w.TaskProgressId))
                .ToListAsync();

            //If the record exists change state to modified so that it will update and not insert.
            foreach (var task in existingTasks)
            {
                _chugDb.Entry(task).State = EntityState.Modified;
            }

            foreach (var history in existingSessionHistory)
            {
                _chugDb.Entry(history).State = EntityState.Modified;
            }

            foreach (var shift in existingEquipmentShifts)
            {
                _chugDb.Entry(shift).State = EntityState.Modified;
            }

            foreach (var progress in exisitngTaskProgress)
            {
                _chugDb.Entry(progress).State = EntityState.Modified;
            }

            rowsSaved += await _chugDb.SaveChangesAsync();

            return rowsSaved;
        }

        #endregion

        #region Private

        RbaFlatData FlattenData(RbaData rbaData)
        {
            //First put each related entity in its own collection while removing dupliaces. 

            //Task
            var tasks = rbaData.Tasks
                .GroupBy(g => g.TaskId)
                .Select(s => s.First())
                .ToList();
            var relatedEquipment = rbaData.Tasks
                .Where(w => w.Equipment != null)
                .Select(t => t.Equipment)
                .GroupBy(g => g.EquipmentId)
                .Select(s => s.First())
                .ToList();
            var relatedLocations = rbaData.Tasks
                .Where(w => w.Location != null)
                .Select(t => t.Location)
                .GroupBy(g => g.LocationId)
                .Select(s => s.First())
                .ToList();
            var supportEquipment = rbaData.Tasks
                .Where(w => w.SupportingEquipment != null)
                .Select(t => t.SupportingEquipment)
                .GroupBy(g => g.SupportingEquipmentId)
                .Select(s => s.First())
                .ToList();
            var operators = rbaData.Tasks
                .Where(w => w.Operator != null)
                .Select(t => t.Operator)
                .GroupBy(g => g.OperatorId)
                .Select(s => s.First())
                .ToList();
            operators.AddRange(rbaData.Tasks.Where(w => w.Supervisor != null).Select(t => t.Supervisor).ToList());
            
            //Sessions History
            var sessionHistory = rbaData.SessionHistory.GroupBy(g => g.SessionHistoryId).Select(s => s.First()).ToList();
            relatedEquipment.AddRange(sessionHistory.Where(w => w.Equipment != null).Select(s => s.Equipment));
            operators.AddRange(rbaData.SessionHistory.Where(w => w.Owner != null).Select(t => t.Owner).ToList());

            //Equipment Shift
            var equipmentShifts = rbaData.EquipmentShifts.GroupBy(g => g.EquipmentShiftId).Select(s => s.First()).ToList();
            relatedEquipment.AddRange(equipmentShifts.Where(w => w.Equipment != null).Select(s => s.Equipment));
            operators.AddRange(equipmentShifts.Where(w => w.Owner != null).Select(s => s.Owner));
            operators.AddRange(equipmentShifts.SelectMany(s => s.Activities).Where(w => w.Owner != null).Select(s => s.Owner));

            //Task Progress
            var taskProgress = rbaData.TaskProgress.GroupBy(g => g.TaskProgressId).Select(s => s.First()).ToList();
            relatedEquipment.AddRange(taskProgress.Where(w => w.Equipment != null).Select(s => s.Equipment));
            relatedLocations.AddRange(taskProgress.Where(w => w.Location != null).Select(s => s.Location));
            tasks.AddRange(taskProgress.Where(w => w.Task != null).Select(s => s.Task));
            operators.AddRange(taskProgress.Where(w => w.Owner != null).Select(s => s.Owner));

            //Null out the related entities to avoid duplicate inserting. The relationship is still preserved via the foreign key
            foreach (var task in tasks)
            {
                task.Equipment = null;
                task.Location = null;
                task.SupportingEquipment = null;
                task.Operator = null;
                task.Supervisor = null;
            }

            foreach (var session in sessionHistory)
            {
                session.Owner = null;
                session.Equipment = null;
            }

            foreach (var shift in equipmentShifts)
            {
                shift.Owner = null;
                shift.Equipment = null;
            }

            foreach (var progress in taskProgress)
            {
                progress.Task = null;
                progress.Location = null;
                progress.Equipment = null;
                progress.Owner = null;
            }

            var flattenedData = new RbaFlatData
            {
                Tasks = tasks.GroupBy(g => g.TaskId).Select(s => s.First()).ToList(),
                Operators = operators.GroupBy(g => g.OperatorId).Select(s => s.First()).ToList(), //Grouping to remove duplicates
                RelatedEquipment = relatedEquipment.GroupBy(g => g.EquipmentId).Select(s => s.First()).ToList(), //Grouping to remove duplicates
                RelatedLocations = relatedLocations.GroupBy(g => g.LocationId).Select(s => s.First()).ToList(),
                SupportingEquipment = supportEquipment,
                SessionHistory = sessionHistory,
                EquipmentShifts = equipmentShifts,
                TaskProgress = taskProgress
            };

            return flattenedData;
        }

        #endregion

        #endregion

    }
}
