using System;
using Microsoft.Extensions.Configuration;
using Xunit;
using Moq;
using CDP.ETL.Data;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace CDP.ETL.UnitTests
{
    public class RbaDataSourceTest
    {
        [Fact]
        public void ProsessRbaData_ShouldAddToTaskList()
        {
            //Arrange
            var config = new Mock<IConfiguration>();
            var rbaDataSourceSut = new RBADataSource(config.Object);
            var rawData = new List<JObject>();
            var test1 = new JObject();
            var test2 = new JObject();
            var test3 = new JObject();

            test1["type"] = "_gh_task";
            test2["type"] = "_gh_task";

            rawData.Add(test1);
            rawData.Add(test2);
            rawData.Add(test3);

            //Act
            var rbaData = rbaDataSourceSut.ProsessRbaData(rawData);

            //Assert
            Assert.Equal(2, rbaData.Tasks.Count);
        }

        [Fact]
        public void ProsessRbaData_ShouldAddToSessionHistoryList()
        {
            //Arrange
            var config = new Mock<IConfiguration>();
            var rbaDataSourceSut = new RBADataSource(config.Object);
            var rawData = new List<JObject>();
            var test1 = new JObject();
            var test2 = new JObject();
            var test3 = new JObject();

            test1["type"] = "_gh_session_history";
            test2["type"] = "_gh_session_history";

            rawData.Add(test1);
            rawData.Add(test2);
            rawData.Add(test3);

            //Act
            var rbaData = rbaDataSourceSut.ProsessRbaData(rawData);

            //Assert
            Assert.Equal(2, rbaData.SessionHistory.Count);
        }

        [Fact]
        public void ProsessRbaData_ShouldAddToEquipmentShiftList()
        {
            //Arrange
            var config = new Mock<IConfiguration>();
            var rbaDataSourceSut = new RBADataSource(config.Object);
            var rawData = new List<JObject>();
            var test1 = new JObject();
            var test2 = new JObject();
            var test3 = new JObject();

            test1["type"] = "_gh_equipment_shift";
            test2["type"] = "_gh_equipment_shift";

            rawData.Add(test1);
            rawData.Add(test2);
            rawData.Add(test3);

            //Act
            var rbaData = rbaDataSourceSut.ProsessRbaData(rawData);

            //Assert
            Assert.Equal(2, rbaData.EquipmentShifts.Count);
        }
    }
}
