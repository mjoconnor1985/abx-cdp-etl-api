﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CDP.ETL.Domain
{
    public interface IChugMobileEtlService
    {
        Task<int> ExecuteRbaToChugMobileEtlAsync(DateTime date);
    }
}
