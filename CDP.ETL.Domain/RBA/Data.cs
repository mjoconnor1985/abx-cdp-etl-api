﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Domain.RBA
{
    public class Data
    {
        public string Quantity { get; set; }

        public string Type { get; set; }
    }
}
