﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CDP.ETL.Data;

namespace CDP.ETL.Domain.RBA
{
    public class RBAService : IRBAService
    {
        #region Fields

        IRBADataSource _rbaDataSource;
        IMapper _mapper;

        #endregion

        #region Constructors

        public RBAService(IRBADataSource rbaDataSource, IMapper mapper)
        {
            _rbaDataSource = rbaDataSource;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<Consumable>> GetConsumablesAsync(DateTime date)
        {
            var consumables = await _rbaDataSource.GetConsumablesAsync(date);
            return _mapper.Map<IEnumerable<Consumable>>(consumables);
        }

        

        #endregion

    }
}
