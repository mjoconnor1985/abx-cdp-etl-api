﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Domain.RBA
{
    public class Task
    {
        public string Id { get; set; }

        public string Operation { get; set; }

        public string Type { get; set; }
    }
}
