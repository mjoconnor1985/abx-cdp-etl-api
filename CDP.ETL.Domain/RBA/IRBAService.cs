﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CDP.ETL.Domain.RBA
{
    public interface IRBAService
    {
        Task<IEnumerable<Consumable>> GetConsumablesAsync(DateTime date);
    }
}
