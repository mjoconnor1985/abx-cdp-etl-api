﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CDP.ETL.Domain.RBA
{
    public class Owner
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string UserName { get; set; }
    }
}
