﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;

namespace CDP.ETL.Domain.RBA
{
    public class Consumable
    {
        public string Id { get; set; }

        public string CreatedAt { get; set; }

        public IEnumerable<Data> Data { get; set; }

        public string Notes { get; set; }

        public Owner Owner { get; set; }

        public Task Task { get; set; }

        public string Type { get; set; }

        public string UpdatedAt { get; set; }


    }
}
