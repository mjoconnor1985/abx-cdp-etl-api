﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CDP.ETL.Data.Chug;
using CDP.ETL.Data;

namespace CDP.ETL.Domain
{
    public class ChugMobileEtlService : IChugMobileEtlService
    {
        #region Fields

        IMapper _mapper;
        IChugMobileDataSource _chugDataSource;
        IRBADataSource _rbaDataSource;

        #endregion

        #region Constructors

        public ChugMobileEtlService(IMapper mapper, IChugMobileDataSource chugDataSource, IRBADataSource rbaDataSource)
        {
            _mapper = mapper;
            _chugDataSource = chugDataSource;
            _rbaDataSource = rbaDataSource;
        }

        #endregion

        #region Methods

        public async Task<int> ExecuteRbaToChugMobileEtlAsync(DateTime date)
        {
            var apiRbaData = await _rbaDataSource.GetRbaDataAsync(date);
            var dbRbaData = _mapper.Map<ETL.Data.Chug.RbaData>(apiRbaData);
            return await _chugDataSource.SaveRbaData(dbRbaData);
        }

        #endregion

    }
}
